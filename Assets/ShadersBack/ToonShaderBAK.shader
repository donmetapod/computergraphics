﻿Shader "Custom/ToonShaderBAK" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RampTex ("Ramp", 2D) = "white" {}
		//_CelShadingLevels ("Levels", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		#pragma surface surf Toon

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
        sampler2D _RampTex;
        
		struct Input {
			float2 uv_MainTex;
		};
	
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		}

		fixed4 LightingToon(SurfaceOutput s, fixed3 lightDir, fixed3 atten){
			half NdotL = dot (s.Normal, lightDir);
			
			//ramp texture
			NdotL = tex2D(_RampTex, fixed2(NdotL, 0.5));
			
			//light snap
			//half cel = floor(NdotL * _CelShadingLevels) / (_CelShadingLevels-0.5);
			
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten);
			c.a = s.Alpha;
			return c;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
