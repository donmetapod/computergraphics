﻿Shader "Custom/NormalMapShader" {
	Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _NormalMap ("Normal map", 2D) = "bump" {}
      _NormalMapIntensity("Normal intensity", Range(0,1)) = 1
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      
      CGPROGRAM
      
      #pragma surface surf Lambert
      
      struct Input {
        float2 uv_MainTex;
        float2 uv_NormalMap;
      };
      
      sampler2D _MainTex;
      sampler2D _NormalMap;
      float _NormalMapIntensity;
      
      void surf (Input IN, inout SurfaceOutput o) {
        o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
        float3 normalMap = UnpackNormal (tex2D (_NormalMap, IN.uv_NormalMap));
        normalMap.x *= _NormalMapIntensity;
        normalMap.y *= _NormalMapIntensity;
        o.Normal = normalize (normalMap);
     
      }
		ENDCG
	}
	FallBack "Diffuse"
}
