﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour
{
	
	private void Start()
	{
		light = GetComponent<Light>();
		StartCoroutine(LightBlink());
	}

	[SerializeField] private Light light;
	[SerializeField] private int numberOfBlinks;
	[SerializeField] private float blinkDelayTime;
	
	IEnumerator LightBlink()
	{
		int counter = 0;
		while(counter < numberOfBlinks){
			yield return new WaitForSeconds(blinkDelayTime);
			light.enabled = !light.enabled;
			if(light.enabled){
				counter++;
			}
		}
		
	}
}
