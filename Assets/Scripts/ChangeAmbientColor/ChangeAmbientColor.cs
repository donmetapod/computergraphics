﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAmbientColor : MonoBehaviour {

	Renderer renderer;

	void Start(){
		renderer = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		float ambient = Mathf.PingPong (Time.time, 10);
		renderer.material.SetFloat ("_AmbientColorSlider", ambient);
	}
}
