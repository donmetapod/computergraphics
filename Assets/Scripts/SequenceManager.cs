﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SequenceManager : MonoBehaviour
{

	[SerializeField] private Text shaderTypeUI;
	[SerializeField] private Text lightUI;
	[SerializeField] private Text colorUI;
	[SerializeField] private Text textureUI;
	[SerializeField] private Text timeUI;
	[SerializeField] private GameObject mainObject;
	[SerializeField] Color currentColor;
	[SerializeField] private Color startColor;
	[SerializeField] private Color targetColor;
	[SerializeField] private Texture2D dotTex;
	[SerializeField] private Texture2D roughTex;
	[SerializeField] private Texture2D roughNormal;
	[SerializeField] private GameObject directionalLight;
	private Renderer mainObjectRenderer;
	[SerializeField] private Shader texturedShader;
	[SerializeField] private Shader normalMapShader;

	[SerializeField] [Range(0, 10)] private float timeBetweenChange = 5;
	
	// Use this for initialization
	void Start ()
	{
		mainObjectRenderer = mainObject.GetComponent<Renderer>();
		texturedShader = Shader.Find("Custom/TextureShader");
		normalMapShader = Shader.Find("Custom/NormalMapShader");
		StartCoroutine(MainSequence());
	}
	
	// Update is called once per frame
	void Update () {
		colorUI.text = "Color: " + currentColor.ToString();
		timeUI.text = "Time: " + Time.time;
		mainObjectRenderer.material.color = currentColor;
	}

	IEnumerator LerpColor(float startTime)
	{
		while (currentColor.r != targetColor.r)
		{
			//print(currentColor.ToString());
			currentColor = Color.Lerp(startColor, targetColor, Time.time - startTime);
			yield return new WaitForEndOfFrame();
			//yield return new WaitForSeconds(0.3f);
		}
		
	}

	IEnumerator LerpNormalIntensity()
	{
		float counter = 0;
		while (counter < 10)
		{
			counter += Time.deltaTime;
			mainObjectRenderer.material.SetFloat("_NormalMapIntensity", counter);
			yield return  new WaitForEndOfFrame();
		}
		
	}

	IEnumerator CameraZoom()
	{
		while (Camera.main.fieldOfView > 20)
		{
			Camera.main.fieldOfView -= Time.deltaTime * 2;
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator MainSequence()
	{	
		yield return new WaitForSeconds(timeBetweenChange);
		directionalLight.SetActive(true);
		lightUI.text = "Light: ON";
		yield return new WaitForSeconds(timeBetweenChange);
		StartCoroutine(LerpColor(Time.time));
		yield return new WaitForSeconds(timeBetweenChange);
		mainObjectRenderer.material.shader = texturedShader;
		shaderTypeUI.text = "Shader: Textured";
		textureUI.text = "Texture: ON";
		mainObjectRenderer.material.SetTexture("_MainTex", dotTex);
		yield return new WaitForSeconds(timeBetweenChange);
		mainObjectRenderer.material.shader = normalMapShader;
		mainObjectRenderer.material.SetTexture("_MainTex", null);
		StartCoroutine(LerpNormalIntensity());
		shaderTypeUI.text = "Shader: NormalMap";
		textureUI.text = "Texture: OFF";
		yield return new WaitForSeconds(timeBetweenChange);
		yield return new WaitForSeconds(timeBetweenChange);
		mainObjectRenderer.material.SetTexture("_NormalMap", null);
		mainObjectRenderer.material.SetTexture("_MainTex", roughTex);
		mainObjectRenderer.material.SetTexture("_NormalMap", roughNormal);
		shaderTypeUI.text = "Shader: Texture + NormalMap";
		textureUI.text = "Texture: ON";
		yield return new WaitForSeconds(timeBetweenChange);
		StartCoroutine(CameraZoom());
	}
}
